## Команды

`qm unlock <vmid>` - разблокировать при зависшем бэкапе.  
  
Ошибка при попытке выключить vm или сделать `qm unlock 102`  
`TASK ERROR: can't lock file '/var/lock/qemu-server/lock-102.conf' - got timeout`  
`rm /var/lock/qemu-server/lock-102.conf`  

## Установка Windows
### Включение balloon, динамическое распределение памяти  
Копируем нужную папку с диска (напр. `D:\Balloon\2k16\amd64`) в `C:\Program Files` и переименовываем amd64 -> Balloon. Запускаем из командной строки `C:\Program Files\Balloon\blnsrv.exe -i`  

### Если не стартует qemu-guest-agent(Error 1053)  
Установить драйвер PCI-контроллер Simple Communication - vioserial  
  
### Windows 2012 guest best practices  
https://pve.proxmox.com/wiki/Windows_2012_guest_best_practices  
VirtIO RedHat controller driver for Hard Disk - amd64/w2k (or w10) 
Balloon, Netkvm,  
  
### Старт VM's без второй ноды в кластере
`cluster not ready - no quorum (500)`  
`pvecm e 1`  
  
## Установка Proxmox на Debian 9
  
1. Есть 4 диска, 2х1Tb и 2х4Tb. Установка Debian на mdadm raid1.
    - Создаем md0 1000Mb из 2х1Tb bootable flag:on primary 
    - Создаем md1 950Gb из 2х1Tb primary
    - md0 делаем /boot ext2
    - в md1 создаем lvm, vggroup vg0p2
    - в vg0p2 создаем две lvgroup, swap 32Gb /swap, root 100Gb /root ext4
  
2. Установим загрузчик на второй диск: `dpkg-reconfigure grub-pc`
  
3. Устанавливаем статический IP-адрес:  
`vim /etc/network/interfaces`
```
iface eth0 inet static
address 192.168.1.1
gateway 192.168.1.2
netmask 255.255.255.0
```
Прописываем адрес в `vim /etc/hosts`  
  
4. Создадим raid-1 из 2х4Tb дисков.  
`mdadm --create --verbose /dev/md127 --level=1 --raid-devices=2 /dev/sdc /dev/sdd`  
  
5. Создадим LVM-Thin для виртуальных машин  
`pvcreate /dev/md127`  
`vgcreate vg1p2 /dev/md127`  
`lvcreate -L 3500G -n data vg1p2`  
`lvconvert --type thin-pool --poolmetadatasize +16G vg1p2/data`  
(или `lvconvert --type thin-pool vg1p2/data` `lvextend --poolmetadatasize +15G vg1p2/data`)  
`mkfs.ext4 /dev/vg1p2/data`  
`mkdir /var/lib/vz`  
`mount /dev/vg1p2/data /var/lib/vz`  

6. Устанавливаем Proxmox.  
Добавляем репозиторий: `echo "deb http://download.proxmox.com/debian/pve stretch pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list`  
(debian 10: `echo "deb http://download.proxmox.com/debian/pve buster pve-no-subscription" > /etc/apt/sources.list.d/pve-install-repo.list`)  
Добавляем цифровую подпись репозитория: `wget http://download.proxmox.com/debian/proxmox-ve-release-5.x.gpg -O /etc/apt/trusted.gpg.d/proxmox-ve-release-5.x.gpg`  
(debian 10: `wget http://download.proxmox.com/debian/proxmox-ve-release-6.x.gpg -O /etc/apt/trusted.gpg.d/proxmox-ve-release-6.x.gpg`)  
`apt update && apt dist-upgrade`  
`apt install proxmox-ve postfix open-iscsi`  
Если не используем dual-boot(вторую систему) то удаляем: `apt remove os-prober`  
  
7. Отключаем платную подписку.  
`vim /etc/apt/sources.list.d/pve-enterprise.list`  
```
#deb https://enterprise.proxmox.com/debian/pve stretch pve-enterprise
deb http://download.proxmox.com/debian/pve stretch pve-no-subscription
```


  


