# Настройка L2TP IPsec подключение client-to-site для RouterOS 6.44

Создадим пул адресов для клиентов:  
`/ip pool add name=l2tp-pool ranges=192.168.70.30-192.168.70.50`  
Настроим профиль для клиентов:  
`/ppp profile add name=l2tpipsec-prof local-address=192.168.70.1 remote-address=l2tp-pool dns-server=192.168.70.1`  
Включим и настроим L2TP-сервер:  
`/interface l2tp-server server set enable=yes default-profile=l2tpipsec-prof authentication=mschap2`  
Настроим IPsec:  
`/ip ipsec policy group add name=l2tp-ipsec`  
`/ip ipsec proposal add name=ipsecl2tp-prop auth-algorithms=sha1,sha256  enc-algorithms=aes-128-cbc,aes-256-cbc pfs-group=modp1024`  
`/ip ipsec policy set [ find default=yes ] src-address=0.0.0.0/0 dst-address=0.0.0.0/0 protocol=all proposal=ipsecl2tp-prop template=yes group=l2tp-ipsec comment="L2TP"`  
`/ip ipsec profile add name=ipsecl2tp-prof enc-algorithm=3des,aes-128,aes-256 dh-group=modp1024 proposal-check=obey`  
`/ip ipsec peer add exchange-mode=main passive=yes name=l2tpserver profile=ipsecl2tp-prof send-initial-contact=no`  
`/ip ipsec identity add generate-policy=port-override auth-method=pre-shared-key secret="Share passwd" policy-template-group=l2tp-ipsec peer=l2tpserver`  

Создадим пользователя и зададим ему пароль:  
`/ppp secret add name="user1" password="User's passwd" service=l2tp profile=l2tpipsec-prof`  
Добавим правила (IKE — UDP:500, IKEv2 — UDP:4500, NAT-T — UDP:4500, ESP — ipsec-esp(50)):  
`/ip firewall filter add chain=input action=accept protocol=udp port=500,4500 comment="Permit L2TP" place-before=1`  
`/ip firewall filter add chain=input action=accept protocol=ipsec-esp comment="Permit L2TP" place-before=2`  
  
На bridge1 не забудем включить proxy-arp для доступа к внутренней сетке внешних клиентов.  
При необходимости ограничим доступ внешних клиентов, разрешим только один внутренний ip-адрес:  
`/ip firewall filter add chain=forward src-address=192.168.70.0 dst-address=!192.168.7.122 action=reject comment="Access rule"`  
  
```    
/ip pool add name=l2tp-pool ranges=192.168.70.30-192.168.70.50  
/ppp profile add name=l2tpipsec-prof local-address=192.168.70.1 remote-address=l2tp-pool dns-server=192.168.70.1  
/interface l2tp-server server set enable=yes default-profile=l2tpipsec-prof authentication=mschap2  
/ip ipsec policy group add name=l2tp-ipsec  
/ip ipsec proposal add name=ipsecl2tp-prop auth-algorithms=sha1,sha256  enc-algorithms=aes-128-cbc,aes-256-cbc pfs-group=modp102  
/ip ipsec policy set [ find default=yes ] src-address=0.0.0.0/0 dst-address=0.0.0.0/0 protocol=all proposal=ipsecl2tp-prop template=yes group=l2tp-ipsec comment="L2TP"  
/ip ipsec profile add name=ipsecl2tp-prof enc-algorithm=3des,aes-128,aes-256 dh-group=modp1024 proposal-check=obey  
/ip ipsec peer add exchange-mode=main passive=yes name=l2tpserver profile=ipsecl2tp-prof send-initial-contact=no  
/ip ipsec identity add generate-policy=port-override auth-method=pre-shared-key secret="Share passwd" policy-template-group=l2tp-ipsec peer=l2tpserver  
/ppp secret add name="user1" password="User's passwd" service=l2tp profile=l2tpipsec-prof  
/ip firewall filter add chain=input action=accept protocol=udp port=500,4500 comment="Permit L2TP" place-before=1  
/ip firewall filter add chain=input action=accept protocol=ipsec-esp comment="Permit L2TP" place-before=2  
```
/ip ipsec policy set [ find default=yes ] src-address=0.0.0.0/0 dst-address=0.0.0.0/0 protocol=all proposal=proposal3-l2tpipsec template=yes group=group5-l2tpipsec comment="L2TP"