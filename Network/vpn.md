## OpenVPN Site to Site PfSense & Mikrotik

Настройка OpenVPN канала Peer to Peer (SSL/TLS) по протоколу TCP между двумя локальными сетями. Первая находится за PfSense 2.3.4 (server) с сеткой 192.168.0.0/24, вторая за Mikrotik 951Ui-2HnD (client) с сеткой 192.168.11.0/24. 

1. Сгенерируем сертификаты на стороне PfSense:   *System -> Cert. Manager -> Certificates*. Добавим новый сертификат для сервера: *Add*  
<img src="Network/pictures/ovpn01.jpg" width="800">  

<img src="Network/pictures/ovpn02.jpg" width="800">  


Создадим еще один сертификат для клиента:  
<img src="Network/pictures/ovpn03.jpg" width="800">  

2. Сконфигурируем OpenVPN Server: *VPN -> OpenVPN -> Servers -- Add*  
<img src="Network/pictures/ovpn04.jpg" width="500">  
Server mode - Peer to Peer (SSL/TLS) (Shared Key не поддерживается Mikrotik), Protocol - TCP (UDP не поддерживается на стороне Mikrotik), Local port - 1195 (Default - 1194), TLS authentication - Disable, Server certificate - Сертификат сгенерированный для сервера (первым).  
<img src="Network/pictures/ovpn05.jpg" width="500">  
Добавим маршруты в *Custom options:*  
<img src="Network/pictures/ovpn06.jpg" width="500">   
и на вкладке: *VPN -> OpenVPN -> Client Specific Overrides -- Add*  
<img src="Network/pictures/ovpn07.jpg" width="500"> 
Server List - выбираем сервер сконфигурированный для этой задачи, Common name - такое же как в поле "CN=" сертификата.

3. Создадим правило Firewall для пропуска трафика: *Firewall -> NAT -> Port Forward -- Add*  
<img src="Network/pictures/ovpn08.jpg" width="500"> 
В поле Source указываем внешний IP-адрес Mikrotik.

4. Экспортируем 3 сертификата: *System -> Cert. Manager -> CAs.* Кнопка: *Export CA*  
Переходим на вкладку Certificates. Жмем Export Certificate и Export Key для сертификата сгенерированного для клиента.  
<img src="Network/pictures/ovpn09.jpg" width="500"> 
Получаем 3 файла и переходим к настройке Mikrotik.  
<img src="Network/pictures/ovpn10.jpg" width="500"> 

5. Закачаем 3 сертификата на Mikrotik через утилиту WinBox перетащив их в окно File:
<img src="Network/pictures/ovpn11.jpg" width="500">  
теперь их установим. System -> Certificates -- Import. Добавляем сертификат клиента, пароль не вводим:  
<img src="Network/pictures/ovpn12.jpg" width="500">  
дальше добавляем ключ, если все верно, то в первом столбце буква T поменяется на KT. Добавляем оставшейся сертификат CA:  
<img src="Network/pictures/ovpn13.jpg" width="500">  

6. Переходим к настройке OpenVPN на стороне Mikrotik. Заходим в меню PPP, вкладка Profiles. Создать новый:  
<img src="Network/pictures/ovpn14.jpg" width="500">  
Переходим на вкладку Interface и создаем новый: OpenVPN Client.  
<img src="Network/pictures/ovpn15.jpg" width="500">
<img src="Network/pictures/ovpn16.jpg" width="500">  
Connect To - Внешний IP-адрес PfSense Server, Certificate - Сертификат Клиента  
7. Добавим правило для пропуска трафика. Заходим IP -> Firewall -> NAT добавить правило:  
<img src="Network/pictures/ovpn17.jpg" width="500">  
<img src="Network/pictures/ovpn18.jpg" width="500"> 
На этом настройка OpenVPN канала Site to Site закончена.  