# Настройка L2TP IPsec подключение client-to-site для RouterOS 6.43

```  
/ip pool add name=l2tp-pool ranges=192.168.130.30-192.168.130.50  
/ppp profile add name=l2tpipsec-prof change-tcp-mss=yes use-upnp=no use-mpls=no use-compression=no use-encryption=no only-one=yeslocal-address=192.168.130.1 remote-address=l2tp-pool dns-server=192.168.130.1  
/interface l2tp-server server set authentication=mschap2 enabled=yes default-profile=l2tpipsec-prof use-ipsec=no caller-id-type=ip-address  
/ppp secret add name=l2tp-user password="123" service=l2tp profile=l2tpipsec-prof  
/ip ipsec policy group add name=l2tp-ipsec  
/ip ipsec proposal add name=ipsecl2tp-prop auth-algorithms=sha1 enc-algorithms=aes-128-cbc,aes-256-cbc pfs-group=modp1024  
/ip ipsec peer profile add name=ipsecl2tp-prof enc-algorithm=3des,aes-128,aes-256 dh-group=modp1024 proposal-check=obey  
/ip ipsec peer add profile=ipsecl2tp-prof auth-method=pre-shared-key exchange-mode=main passive=yes policy-template-group=l2tp-ipsec send-initial-contact=no generate-policy=port-strict secret="topsecret"  
/ip firewall filter add chain=input action=accept protocol=udp port=1701,500,4500 comment="Permit L2TP"  
/ip firewall filter add chain=input action=accept protocol=ipsec-esp comment="Permit L2TP"  
```



