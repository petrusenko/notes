## Commands
`ciscoasa(config)# ssh timeout 30` -  
`ciscoasa(config)# clear configure all`  
  
<img src="Network/pictures/asa01.png" width="500">  

  
## Allow icmp
```
ciscoasa(config)# policy-map global_policy  
ciscoasa(config-pmap)# class inspection_default  
ciscoasa(config-pmap-c)# inspect icmp  
ciscoasa(config-pmap-c)# inspect icmp error  
ciscoasa(config)# show run policy-map  
```

### Allow ping
```
ciscoasa(config)# object network nat1  
ciscoasa(config)# subnet 10.1.1.0 255.255.255.0  
ciscoasa(config)# nat (inside,outside) dynamic interface  
ciscoasa(config)# access-list internet-in permit icmp any any echo-reply  
ciscoasa(config)# access-group internet-in in interface outside  
```
  
###  
`ciscoasa(config)# access-list inside-in permit udp any gt 1024 host 8.8.8.8 eq domain`  
`ciscoasa(config)# access-group inside-in in interface inside`  
  
`ciscoasa(config)# access-list inside-in line 1 extended permit ip any gt 1024 208.83.0.0 255.255.252.0 log errors`  
`ciscoasa(config)# logging timestamp`  
  
*DNS*    
`ciscoasa(config)# dns domain-lookup internet`  
`ciscoasa(config)# dns name-server 8.8.8.8`  
  
*NAT*      
`ciscoasa(config)# object network OBJ_NAT_LAN`  
`ciscoasa(config-network-object)# subnet 10.1.1.0 255.255.255.0`  
`ciscoasa(config-network-object)# nat (inside,outside) dynamic interface`   
  
### Debug
`ciscoasa(config)# packet-tracer input inside icmp 10.1.1.6 8 0 8.8.8.8`  
`ciscoasa(config)# packet-tracer input inside tcp 10.1.1.6 30000 24.33.44.22 http`  
  
`ciscoasa(config)# show conn`  
`ciscoasa(config)# capture test1 inreface inside`  
`ciscoasa(config)# no capture test1`  
  
### notes(vpn)
*ASA*  
`ciscoasa(config)#  route outside 0 0 210.210.1.1`  
`ciscoasa(config)# class-map inspection_default`  
`ciscoasa(config-cmap)# match default-inspection-traffic`  
`ciscoasa(config)# policy-map global_policy`  
`ciscoasa(config-pmap)# class inspection_default`  
`ciscoasa(config-pmap-c)# inspect icmp`  
`ciscoasa(config)# service-policy global_policy global`  
*Router(aka internet)*  
`router(config)# int fa0/0`  
`router(config-if)# ip address 210.210.1.1 255.255.255.0`  
`router(config-if)# no shutdown`  
`router(config)# int fa0/1`  
`router(config-if)# ip address 210.210.2.1 255.255.255.0`  
`router(config-if)# no shutdown`  
  
*NAT*  
  
*VPN*  
Типовые настройки МЭ:  
Настройка первой фазы  
crypto ikev1 enable outside  
crypto ikev1 policy 1  
encr 3des  
hash md5  
authentication pre-share  
group 2  
lifetime 43200  
Настройка ключа аутентификации и пира  
tunnel-group 210.210.2.2 type ipsec-l2l  
tunnel-group 210.210.2.2 ipsec-attributes  
ikev1 pre-shared-key cisco  
Вторая фаза  
crypto ipsec ikev1 transform-set TS esp-3des esp-md5-hmac  
Определяем какой трафик шифровать  
access-list FOR-VPN extended permit icmp 192.168.1.0  
255.255.255.0 192.168.2.0 255.255.255.0  
Создание криптокарты  
crypto map To-Site2 1 match address FOR-VPN  
crypto map To-Site2 1 set peer 210.210.2.2  
crypto map To-Site2 1 set security-association lifetime seconds  
86400  
crypto map To-Site2 1 set ikev1 transform-set TS  
Привязка к интерфейсу  
crypto map To-Site2 interface outside  
  
(asa 192.168.1.1) access-list FROM-VPN permit icmp 192.168.2.0 255.255.255.0 192.168.1.0 255.255.255.0  
(asa 192.168.1.1) access-group FROM-VPN out interface inside  
  
*debug*  
show crypto isakmp sa  
show crypto ipsec sa  