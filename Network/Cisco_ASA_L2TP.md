### Настройка L2TP IPsec VPN канала на Cisco ASA 5505
  
Настром Cisco 5505 на подключения внешних сотрудников через встроенный VPN-клиент Windows. GUI ASDM.  
  
1. Запускаем VPN-Wizzard в ASDM, выбираем IPsec.  
<img src="Network/pictures/c_l2tp01.jpeg" width="800">  
  
2. Выбираем внешний интерфейс.  
<img src="Network/pictures/c_l2tp02.jpeg" width="800">
  
3. Выбираем L2TP over IPsec и оставляем только протокол MS-CHAP-V2  
<img src="Network/pictures/c_l2tp03.jpeg" width="800">  
  
4. Устанавливаем предварительный ключ аутентификации.  
<img src="Network/pictures/c_l2tp04.jpeg" width="800">  
  
5. Выбираем внутреннюю базу с пользователями.  
<img src="Network/pictures/c_l2tp05.jpeg" width="800">  
  
6. Выбираем или создаем новый пул ip-адресов для vpn-клиентов.  
<img src="Network/pictures/c_l2tp06.jpeg" width="800">  

7. Добавляем пользователей, если нужно.  
<img src="Network/pictures/c_l2tp07.jpeg" width="800">  
  
8. Добавляем DNS-сервера и название домена(опционально).  
<img src="Network/pictures/c_l2tp08.jpeg" width="800">  
  
9. Устанавливаем внутреннюю сеть, в которой будут клиенты. Устанавливаем\снимаем следующие чекбоксы.  
<img src="Network/pictures/c_l2tp09.jpeg" width="800">  
  
10. Заходим в *Configuration -> Firewall -> Objects -> Local Users* и делаем отметку у нужного пользователя, что он будет логиниться с помощью протокола MSCHAP. А так же ограничиваем ему доступ, если надо.  
<img src="Network/pictures/c_l2tp10.jpeg" width="800">  