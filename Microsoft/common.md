## Общие заметки

### Оглавление:
1. [Загрузочная флешка UEFI](#boot_usb)
2. [Проблемы с VipNet, Outlook, 1C](#vipnet)  
3. [Копирование с сохранением разрешений NTFS](#copyACL)

### Загрузочная флешка UEFI<a name="boot_usb"></a>
Dell с новым bios, например Vostro 3470. Дает установить Windows 10 в Legacy режиме, но потом не может загрузиться. Нужно устанавливать в режиме UEFI. 
Для этого создаем загрузочную флешку стандартной Windows-программой: **MediaCreationTool1809**. Отключаем в биосе режим Secure boot.  
  
### Проблемы с VipNet, Outlook, 1C<a name="vipnet"></a>
**Windows Server 2012R2**  
**Outlook 2016**  
**VipNet 4.2**  
**1C 8.3**  

С установленной галкой **"Включить поддержку работы VipNet CSP через MS Crypto API"**  
  
<img src="Microsoft/pictures/vipnet1.jpeg" width="500">  
  

письма из Outlook не уходят с ошибкой: **Ошибка: Задача имяпочты отправка' сообщила об ошибке (0x800CCC13) : 'Невозможно подключиться к сети. Проверьте свое сетевое подключение или модем.'**  
  
<img src="Microsoft/pictures/vipnet3.jpeg" width="500">  

Если снять галку, то перестают работать сертификаты **VipNet**. И соответственно не отправляется отчетность 1С.  
  
<img src="Microsoft/pictures/vipnet2.jpeg" width="500">  

Для решения данной проблемы, нужно внести изменения в реестр:
```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Infotecs\PatchEngine\x64\Handlers\{280C07B8-A581-4b80-B55D-9B06ADED53CA}]
"ProcessExclude"="slsvc.exe,virtualbox.exe,outlook.exe"

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Infotecs\PatchEngine\x64\Handlers\{3BF58DD6-A365-4273-8572-6E35AEBE687F}]
"ProcessExclude"="slsvc.exe,virtualbox.exe,outlook.exe"

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Infotecs\PatchEngine\x64\Handlers\{ED1CE025-1172-4742-9F0D-08FB1C94F2B1}]
"ProcessExclude"="slsvc.exe,virtualbox.exe,outlook.exe"

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Infotecs\PatchEngine\x86\Handlers\{280C07B8-A581-4b80-B55D-9B06ADED53CA}]
"ProcessExclude"="slsvc.exe,virtualbox.exe,outlook.exe"

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Infotecs\PatchEngine\x86\Handlers\{3BF58DD6-A365-4273-8572-6E35AEBE687F}]
"ProcessExclude"="slsvc.exe,virtualbox.exe,outlook.exe"

[HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Infotecs\PatchEngine\x86\Handlers\{ED1CE025-1172-4742-9F0D-08FB1C94F2B1}]
"ProcessExclude"="slsvc.exe,virtualbox.exe,outlook.exe"
```

После перезагрузки обе функции будут работать с установленной галкой.  
P.S. Может потребоваться поиск дополнительных ключей реестра по имени ключа (например: 280C07B8-A581-4b80-B55D-9B06ADED53CA) и добавления к ним исключения outlook.exe  
  
### Копирование с сохранением разрешений NTFS<a name="copyACL"></a>
`xcopy D:\bases1C \\NAS\backup1c /E /O`  