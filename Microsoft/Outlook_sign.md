## Корпоративная подпись Outlook (VBScript)

Создание и применение корпоративной подписи силами VBS и групповых политик.

1. Для начала нужно внести актуальные данные для каждого пользователя в атрибутах active directory.
2. Напишем скрипт на Visual Basic:  
```
On Error Resume Next

Set objSysInfo = CreateObject("ADSystemInfo")
strUser = objSysInfo.UserName
Set objUser = GetObject("LDAP://" & strUser)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

strRegard = "C уважением,"
strBlank =""
'Получаем полное имя (Поле AD: Выводимое имя)
strName = objUser.FullName
'Должность
strTitle = objUser.Title
'Компания (Организация)
strCompany = objUser.Company
'Номер телефона
strPhone = objUser.telephoneNumber
'Добавочный (IP-телефон)
strIPphone = objUser.ipPhone
'Дополнительный номер телефона (Факс)
strFax = objUser.facsimileTelephoneNumber
'Номер мобильного телефона
strMobile = objUser.mobile
'Получаем почтовый индекс
strPostIndex = ObjUser.postalCode
'Город
strCity = objuser.l
'Улица
strStreet = objuser.streetAddress
'Адрес электронной почты
strEmail = objuser.mail
'WEB страница
strWeb = objuser.wWWHomePage
'Ссылка на уведомление
strNotice = "http://www..."
'Логотип организации (предварительно поместим файл с логотипом в нужную политику)
strLogo = "\\corp.ru\SYSVOL\corp.ru\Policies\{ID политики}\User\Documents & Settings\logo_mail.jpg"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Set objWord = CreateObject("Word.Application")
Set objDoc = objWord.Documents.Add()
Set objSelection = objWord.Selection
Set objEmailOptions = objWord.EmailOptions
Set objSignatureObject = objEmailOptions.EmailSignature
Set objSignatureEntries = objSignatureObject.EmailSignatureEntries
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'Жирный шрифт - выкл, подчеркнутый -выкл
objSelection.Font.Bold = False
objSelection.Font.Underline = False
'Шрифт
objSelection.Font.Name = "Calibri Light"
'Размер
objSelection.Font.Size = "12"
'Цвет
objSelection.Font.Color = RGB(31, 56, 100)

'С уважением,
objSelection.TypeText strRegard
objSelection.TypeText CHR(11)

'пустая строка
objSelection.TypeText strBlank
objSelection.TypeText CHR(11)

'Вставляем полное имя
objSelection.TypeText strName
objSelection.TypeText CHR(11)

'Должность
objSelection.TypeText strTitle
objSelection.TypeText CHR(11)

'Наименование Компании
objSelection.TypeText strCompany
objSelection.TypeText CHR(11)

'Телефон
objSelection.TypeText "Тел. " & strPhone '
'Добавочный, если есть
if strIPphone <> "" Then
objSelection.TypeText " доб. " & strIPphone
End IF
objSelection.TypeText CHR(11)

'Дополнительный телефон, если есть
if strFax <> "" Then
objSelection.TypeText "        " & strFax
objSelection.TypeText CHR(11)
End IF

'Сотовый, если есть
if strMobile <> "" Then
objSelection.TypeText "Моб. " & strMobile
objSelection.TypeText CHR(11)
End IF

'Вставляем адрес почты
objSelection.TypeText "E-mail: "
objSelection.Font.Name = "Calibri Light"
Set hyp = objSelection.Hyperlinks.Add(objSelection.Range, "mailto:" & strEmail, , , strEmail)
hyp.Range.Font.Size = "12"
objSelection.TypeText CHR(11)

'Почтовый адрес: индекс, город.
objSelection.TypeText strPostIndex & ", " & strCity
objSelection.TypeText CHR(11)

'Почтовый адрес: дом, улица
objSelection.TypeText strStreet
objSelection.TypeText CHR(11)

'Корпоративный сайт
objSelection.Font.Name = "Calibri Light"
Set hyp = objSelection.Hyperlinks.Add(objSelection.Range, strWeb, "", "", strWeb)
hyp.Range.Font.Size = "12"
objSelection.TypeText CHR(11)

'пустая строка
'objSelection.TypeText strBlank
'objSelection.TypeText CHR(11)

'Логотип компании
objSelection.InlineShapes.AddPicture(strLogo)
objDoc.Hyperlinks.Add objDoc.InlineShapes.Item(1), "http://ссылка на сайт"

'пустая строка
objSelection.TypeText strBlank
objSelection.TypeText CHR(11)

'уведомление
objSelection.Font.Size = "10"
objSelection.Font.Underline = True
objSelection.TypeText "Уведомление о конфиденциальности:"
objSelection.Font.Underline = False
objSelection.TypeText " текст: "
objSelection.Font.Name = "Calibri Light"
Set hyp = objSelection.Hyperlinks.Add(objSelection.Range, "ссылка", "", strNotice)
hyp.Range.Font.Size = "10"
objSelection.TypeText CHR(11)
objSelection.Font.Underline = True
objSelection.TypeText "Confidential notice:"
objSelection.Font.Underline = False
objSelection.TypeText " text "
objSelection.Font.Name = "Calibri Light"
Set hyp = objSelection.Hyperlinks.Add(objSelection.Range, "link", "", strNotice)
hyp.Range.Font.Size = "10"
objSelection.TypeText " text."
objSelection.TypeText CHR(11)
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Set objSelection = objDoc.Range()
objSignatureEntries.Add "Corp. Signature", objSelection
objSignatureObject.NewMessageSignature = "Corp. Signature"
objSignatureObject.ReplyMessageSignature = "Corp. Signature"

objDoc.Saved = True
objDoc.Close
objWord.Quit
```

3. Создадим новую групповую политику. Пропишем в автозагрузку для пользователя (Logon scripts) наш vbs-скрипт. Предварительно скопировав его в эту политику для удобства (\\domen.ru\SYSVOL\domen.ru\Policies\{ID политики}\User\Scripts\Logon). 

Создадим новый элемент реестра для назначения новой подписи:  

<img src="Microsoft/pictures/out_sign1.jpg" width="500">  

Путь раздела: Software\Microsoft\Office\16.0\Common\MailSettings . Где 16.0 версия Office 2016 (Office 2013 - 15.0). Corp. Signature - имя подписи.  

Этот элемент реестра удаляем, если хотим разрешить менять подписи.  

<img src="Microsoft/pictures/out_sign2.jpg" width="500">  

Путь: Software\Microsoft\Office\16.0\Outlook\Setup