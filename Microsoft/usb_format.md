# Форматирование флешки с RAW разделами в NTFS\FAT
  
Открываем **cmd** от имени администратора.  
```
diskpart  
list disk  
select disk N  
clean  
create partition primary  
format fs=ntfs\fat32  
assign letter=E
```
