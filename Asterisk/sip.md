## Настройки для подключения к провайдеру mango.ru

```
allowguest=no
register=Login:Password@ID.mangosip.ru/mango

[mango]
type=peer
secret=Password
username=Login
host=ID.mangosip.ru
fromuser=Login
fromdomain=ID.mangosip.ru
insecure=invite
nat=force_rport
context=Context name
dtmfmode=inband
qualify=yes

[number]
host=dynamic
type=friend
secret=Password
qualify=yes
directmedia=no
context=internal
disallow=all
allow=alaw
allowguest=no
dtmfmode=inband
alwaysauthreject=yes
```

## Westcall
```
[general]  
context=public  
register=username:Password:username@uc.westcall.net:9955/westcall-number  
alwaysayreject=yes  
allowguest=no        
  
[westcall-number]  
type=friend  
context=gorod  
username=username  
fromuser=username  
authname=username  
secret=Password  
fromdomain=uc.westcall.net  
host=uc.westcall.net  
insecure=port,invite  
disallow=all  
allow=ulaw  
qualify=yes  
```
