## Установка Asterisk 13 на CentOS 7

### Подготовка
`yum update`  
`yum install epel-release`  
`yum install vim`  
`yum install mc`  
`yum install wget`  

SELinux  
`sestatus`  
`vim /etc/sysconfig/selinux`  
```
SELINUX=disabled
```

Выключаем firewall  
`systemctl disable firewalld`  
`iptables -nL`  

`reboot` || `init 6`  
***

### Скачиваем, собираем.
`cd /usr/src`  
`wget http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-13-current.tar.gz`  
`tar xvzf asterisk-13-current.tar.gz`  
`cd asterisk-13....`  
`./configure`  
Доставляем пакеты:  
`yum install gcc`  
`yum install gcc-c++`  
`yum -y install ncurses-devel`  
`yum -y install libuuid-devel`  
`yum -y install jansson-devel`  
`yum -y install libxml2-devel`  
`yum -y install sqlite-devel`   
(смотрим на http://linux.mixed-spb.ru/asterisk/install_troubles.php)  
Если надо добавить что-то: `make menuselect`  
`make && make install && make samples && make config`  
(make config закинет скрипт инициализации в /etc/init.d)  




