## Оглавление

1. [Неправильная кодировка русских символов Linux(Debian)](#Encoding)
2. [Изменение сетевых настроек](#ChangeIP)
3. [Ошибка загрузки FreeNAS на HP Server(pci error)](#FreeNAS_HP)

### Неправильное отображение русских символов в Linux(Debian)<a name="Encoding"></a>
Отображение знаков "????" вместо русских символов.  
Индикатором так же служит появление ошибки (например при установке новых пакетов):  
>perl: warning: Please check that your locale settings   
>perl: warning: Please check that your locale settings

Для решения:  
`sudo apt-get install locales`  
`sudo dpkg-reconfigure locales`  

Выбираем:
```
en_US.UTF-8
ru_RU.UTF-8
```

### CentOS изменение сетевых настроек<a name='ChangeIP'></a>
Правим файл: `/etc/sysconfig/network-scripts/ifcfg-eth0`
```
HWADDR="FE:35:C3:A9:07:AB"
TYPE="Ethernet"
BOOTPROTO=none #если dhcp, то "dhcp". IPADDR,PREFIX,GATEWAY,DNS - удаляем
IPADDR=192.168.11.210
PREFIX=24
GATEWAY=192.168.11.10
DNS1=192.168.11.1
DEFROUTE="yes"
PEERDNS="yes"
PEERROUTES="yes"
IPV4_FAILURE_FATAL="no"
IPV6INIT="yes"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_PEERDNS="yes"
IPV6_PEERROUTES="yes"
IPV6_FAILURE_FATAL="no"
NAME="eth0"
UUID="fe98cb05-4f70-4dd7-9a22-79719aeaa74a"
ONBOOT="yes"
```

Перезапускаем сеть: `/etc/init.d/network restart`  

### Ошибка загрузки FreeNAS на HP Server(pci error)<a name='FreeNAS_HP'></a>  
Ошибка при установке\загрузке FreeNAS 11 на HP:
```
pcib0: _OSC returned error 0x10
pci0: <ACPI PCI bus> on pcib0
```

<img src="Linux/pictures/freenas2.jpg" width="500">  

При загрузке жмем "3 Configure Boot Options" -> "Esc" -> `set hw.pci.realloc_bars=1` -> `boot`  
Дальше в GUI заходим: System -> Tunables -> Add Tunable:
  
<img src="Linux/pictures/freenas.png" width="500"> 
