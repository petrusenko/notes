## Настройка Debian9 на бэкап общей папки Windows с помощью rsync.

1. Устанавливаем **rsync:**  
`sudo apt-get update`  
`sudo apt-get install rsync`

2. Правим файл конфига **rsync:**  
`sudo mcedit /etc/default/rsync`  
```
RSYNC_ENABLE=true
```

3. Запускаем **rsync:**  
`sudo /etc/init.d/rsync start`

4. Для доступа к расшаренному ресурсу на удаленной системе нам нужно установить пакет **cifs-utils:**  
`sudo apt-get install cifs-utils`  

5. Для правильного отображения русских символов установим пакет *locales:*  
`sudo apt-get install locales`  
`sudo dpkg-reconfigure locales`  

Выбираем:
```
en_US.UTF-8
ru_RU.UTF-8
```

6. Создадим необходимые директории:  
`sudo mkdir /backup`  
`sudo mkdir /backup/dc01`  
`sudo mkdir /backup/current`  
`sudo mkdir /mnt/inbox`

7. Создадим файл с логином и паролем на удаленную систему и внесем в него данные пользователя, которого предварительно завели на удаленной системе:  
`sudo mcedit /root/.smbcredentials`  
```
username=backup
password=pass
```
Установим права доступа на этот файл только для **root:**  
`sudo chmod 700 /root/.smbcredentials`

8. Скрипт для бэкапа:  
`sudo mcedit /root/bin/dc01.sh`

```bash
#!/bin/bash

# Папка для бэкапа
dst_dir=/backup/

# Папка откуда бэкапим
src_dir=/mnt/inbox

# Имя сервера
srv_name=dc01

date

# Монтируем источник бэкапа
mount -t cifs //192.168.0.2/inbox/test /mnt/inbox -o credentials=/root/.smbcredentials,iocharset=utf8,dir_mode=0777,file_mode=0666

df -h | grep //192.168.0.2/inbox/test

# Если диск примонтировался удачно, то запускаем копирование, если нет — создаем файл с ошибкой
if [ $? -eq 0 ]

then
   echo "Starting BackUp ${srv_name}"
   mkdir -p ${dst_dir}${srv_name}/increment/

   # В папке current хранится актуальная версия файлов, в increment — старые версии измененных файлов
   /usr/bin/rsync -a --delete ${src_dir} ${dst_dir}${srv_name}/current/ --backup --backup-dir=${dst_dir}${srv_name}/increment/${srv_name}-$(date +%d.%m.%s--%H:%M:%S)/

   umount /mnt/inbox

   # Удаляем старые изменения из папки increment старше 30-ти дней
   /usr/bin/find ${dst_dir}${srv_name}/increment/ -maxdepth 1 -type d -mtime +30 -exec rm -rf {} \;
   date
   echo "Finish BackUp ${srv_name}"

else

   echo "Source directory unmount!"
   echo "Fail mount source directory!" >> ${dst_dir}Error-${srv_name}-$(date +%d.%m.%s-%H:%M:%S)

fi
```
9. Делаем его исполняемым:  
`sudo chmod 0744 /root/bin/dc01.sh`

10. Редактируем *crontab* для ежедневного запуска скрипта. Добавляем в конец запись:  
`sudo mcedit /etc/crontab`  
```
30 23   * * * /root   /root/bin/dc01.sh  
```

11. Восстанавливаем:  
`sudo mkdir /mnt/bases`  
`mount -t cifs //192.168.0.25/Base /mnt/bases -o credentials=/root/.smbcredentials,iocharset=utf8,dir_mode=0777,file_mode=0666`