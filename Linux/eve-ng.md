## Настройка EVE-NG
  
Настройка стенда eve-ng. Хост на Proxmox 5.2-1 клиент Ubuntu 18.04
  
### Proxmox
1. Включаем вложенную виртуализацию [официальный сайт](https://pve.proxmox.com/wiki/Nested_Virtualization)  
    Пример для Intel, потребуется перезагрузка хоста (желательно).
    ```
    root@proxmox:~# cat /sys/module/kvm_intel/parameters/nested   
    N  
    # echo "options kvm-intel nested=Y" > /etc/modprobe.d/kvm-intel.conf  
    root@proxmox:~# cat /sys/module/kvm_intel/parameters/nested                    
    Y  
    ```

2. Качаем iso-образ и создаем VM (eve-ng на ubuntu). 2 ядра (тип host), 100 Gb, 8192 Ram  
    <img src="Linux/pictures/eve-ng1.png" width="500">  
    <img src="Linux/pictures/eve-ng2.png" width="500">  
  
3. Пароль root:eve, веб-интерфейс admin:eve  
4. Обновляем систему `sudo apt-get update & upgrade`  
  
### Ubuntu 
1. Устанавливаем [Linux Client integration pack](https://github.com/SmartFinn/eve-ng-integration) для упрощения запуска telnet, wireshark и тд. (Проверял только Firefox).
  
### Добавление образа ASAv
1. Качаем образ с интегрированным asdm, anyconnect, настроенным telnet: [asav971-full.qcow2](https://drive.google.com/drive/folders/0B-5kZl7ixcSKSmw0WWtpLWREMFU)  
2. Создаем папку  `mkdir /opt/unetlab/addons/qemu/asav-971`  
    и копируем образ на хост со своего клиента переименовывая в соответствии с требованиями eve-ng: https://www.eve-ng.net/index.php/documentation/qemu-image-namings/ 
    `scp asav971-full.qcow2 root@192.168.11.67:/opt/unetlab/addons/qemu/asav-971/virtioa.qcow2`  
    Скорректируем права скриптом: `/opt/unetlab/wrappers/unl_wrapper -a fixpermissions`  
  
### Добавляем Cisco IOL
1. Скачиваем файл с поддерживаемой прошивкой в директорию:  `/opt/unetlab/addons/iol/bin`  
2. Корректируем права скриптом: `/opt/unetlab/wrappers/unl_wrapper -a fixpermissions`  
3. Корректируем лицензию.  
- Создаем файл IOUkeygen.py  
```
#! /usr/bin/python
print("*********************************************************************")
print("Cisco IOU License Generator - Kal 2011, python port of 2006 C version")
print("Modified to work with python3 by c_d 2014")
import os
import socket
import hashlib
import struct

# get the host id and host name to calculate the hostkey
hostid=os.popen("hostid").read().strip()
hostname = socket.gethostname()
ioukey=int(hostid,16)
for x in hostname:
 ioukey = ioukey + ord(x)
print("hostid=" + hostid +", hostname="+ hostname + ", ioukey=" + hex(ioukey)[2:])

# create the license using md5sum
iouPad1 = b'\x4B\x58\x21\x81\x56\x7B\x0D\xF3\x21\x43\x9B\x7E\xAC\x1D\xE6\x8A'
iouPad2 = b'\x80' + 39*b'\0'
md5input=iouPad1 + iouPad2 + struct.pack('!i', ioukey) + iouPad1
iouLicense=hashlib.md5(md5input).hexdigest()[:16]

print("\nAdd the following text to ~/.iourc:")
print("[license]\n" + hostname + " = " + iouLicense + ";\n")
print("You can disable the phone home feature with something like:")
print(" echo '127.0.0.127 xml.cisco.com' >> /etc/hosts\n")
```
   
- Запускае его `python2 IOUkeygen.py`  
- Создаем `vim iourc`  и копируем в него вывод  
      
```
    [license]
    eve-ng = e2e16dcf109b65bd;
```
  
### Добавляем Linux\Win
Первый вариант(добавляется полная копия?):
1. Скачиваем дистрибутив в (предварительно создав директорию linux-kali-2020.3): `/opt/unetlab/addons/qemu/linux-kali-2020`  
2. Переименовываем: `mv kali-linux-2020.3-installer-netinst-amd64.iso cdrom.iso`  
3. Корректируем права скриптом (если качали live-дистрибутив, то этот шаг последний): `/opt/unetlab/wrappers/unl_wrapper -a fixpermissions`  
4. Создаем диск: `/opt/qemu/bin/qemu-img create -f qcow2 hda.qcow2 15G`  
5. После завершения установки удаляем\переносим дистрибутив: `rm -f cdrom.iso`  
6. Конвертируем `qemu-img convert -c -O qcow2 /opt/unetlab/tmp/0/9bc7c691-cedc-457e-b6b9-f1f33b9c57a7/1/hda.qcow2 /tmp/hda.qcow2`  
7. Переносим `mv /tmp/hda.qcow2 /opt/unetlab/addons/qemu/linux-kali-2020.3/hda.qcow2`  
8. Корректируем права скриптом: `/opt/unetlab/wrappers/unl_wrapper -a fixpermissions`  
  
Второй вариант(протестировать):
1. Качаем дистрибутив в директорию: `/opt/unetlab/addons/qemu/win-10`  
2. Переименовываем в `cdrom.iso`  
3. Создаем диск: `/opt/qemu/bin/qemu-img create -f qcow2 hda.qcow2 30G`  
4. Корректируем права скриптом: `/opt/unetlab/wrappers/unl_wrapper -a fixpermissions`  
5. Запускаем, останавливаем машину  
6. Находим диск `hda.qcow2` по адресу: `/opt/unetlab/tmp/` - удаляем, копируем на его место `/opt/unetlab/addons/qemu/win-10/hda.qcow2`  
7. Корректируем права скриптом: `/opt/unetlab/wrappers/unl_wrapper -a fixpermissions`  