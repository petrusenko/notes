## NFS

### CentOS 7  
`yum install nfs-utils`    
`systemctl status {nfs,rcpbind,nfslock}` - проверка статуса служб  
`systemctl start nfs-server`  
`systemctl enable {nfs,rcpbind,nfslock}`    
`systemctl restart nfs` - перезапуск сервера  
`systemctl reload nfs` - перечитать конфиг без перезапуска службы  
`/etc/exports` - конфигурационный файл  

Пример записи в конфигурационном файле:  
```
# Хост который может подключиться к NFS: FQDN, имя хоста, IP-адрес
# IP-сети: a.b.c.d/префикс или маска 
# Опции по умолчанию (ro,sync,wdelay,root_squash)
/var/nfs/sample    10.0.0.1
/var/nfs/sample2    10.0.0.1(ro) 10.0.0.2(rw)
/var/nfs/sample3    10.0.0.0/24(rw,sync,no_root_squash)

# Опции (основные):
#    ro/rw 
#    sync/async - синхронный режим записи или асинхронный
#    root_squash - root на клиенте будет иметь права nfsnobody в NFS
#    no_root_squash - root->root в примантированной FS
```

Ключи **exportfs:**  
`exportfs` - без ключей - покажет текущую конфигурацию.  
`-r` - произведет экспорты из `/etc/exports`  
`-v` - подробный вывод  
`-u client_ip:/var/nfs/sample` - удаление экспорта  
  
Правила для **firewalld** (NFSv4, tcp 2049):  
```
firewall-cmd --permanent --add-service=nfs 
firewall-cmd --permanent --add-service=mountd 
firewall-cmd --permanent --add-service=rpc-bind
```
  
Поменяем владельца каталога:  
`chown nfsnobody /var/nfs/sample`  
   
Диагностика:  
`lsmod | grep nfs` - модули ядра  
`ps -A | grep nfs`  
`mount | tail -n1` - опции с которыми примонтировали NFS  

Монтирование:  
`mount server_ip:/var/nfs/sample /mnt/`  

Книги:  
- NFS Illustrated  
- NFS and NIS